package regexp;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regexp {

	
	@SuppressWarnings("serial")
	public static void main(String[] args){
		test1("\\d+", "rge5r43"); //base test
		test1("-?\\d+", "rge5r43"); //1)
		test1("-?\\d+", "rge-5564r43");
		test2("-?\\d+.\\d+(?<!0+)\\d*",new ArrayList<String>(){{
			add("1.00");
			add("1.010");
			add("aa1.00hdt");
			add("3.9009");
		}}); //2)
		test2("[a-zA-Z]+le[a-zA-Z]+",new ArrayList<String>(){{
			add("parlement");
			add("arstrsalearsta");
			add("canard");
			add("le canard boiteux va au parlement");
			add("poulet");
		}});//3)
		
		test2("(?<=\\b[a-zA-Z]+)\\d+(?=[a-zA-Z]+\\b)",new ArrayList<String>(){{
			add("ax25By");
			add("le canard bo1teux");

		}});//4)
		
		test2("(?<=\"args\" ?: ?[)(\\d+,?)+(?=])",new ArrayList<String>(){{
			add("{ \"args\" : [10,20] }");

		}});//5)
		
		test2("\\b[b-df-zB-DF-Z]+\\b",new ArrayList<String>(){{
			add("le canard boiteux aux US");
		}});//7)
		
		test2("\\b(\\w{2,5})(\\1)\\b",new ArrayList<String>(){{
			add("le canard boiteux aux US adsads adsads");
		}});//8)
		
		test2("\\b(?=[a-hj-z]*i[a-hj-z]*)(?=[a-z]*e[a-z]*)[a-z]*\\b",new ArrayList<String>(){{
			add("aar rst ai aeee aieie aieee ");
			add("aar rst ai aeee aieie aeeie ");
		}});//9)
		
		test2("\\b(\\w{2})\\w*(\\1)\\b",new ArrayList<String>(){{
			add("le canard boiteuxbo aux US adsads adsads");
		}});//10)
	}
	
	public static void test1(String param, String param2) {
		String patternString = param;
		String text = param2;
		Pattern p = Pattern.compile(patternString);
		Matcher m = p.matcher(text);
		System.out.println("R.E.: " + patternString);
		System.out.println("Test: " + text);
		boolean found = m.find();
		if (found) {
			System.out.println("Position début : " + m.start());
			System.out.println("Avant : " + text.substring(0,m.start()));
			System.out.println("Sélection : " + m.group());
			// System.out.println("Groupe : " + m.group(1));
			System.out.println("Position fin : " + m.end());
			System.out.println("Après : " + text.substring(m.end()));
			
			}
		System.out.println("\n");
		}
	public static void test2(String param, ArrayList<String> param2) {
		String patternString = param;
		for (int i = 0; i < param2.size(); i++) {
			String text = param2.get(i);
			Pattern p = Pattern.compile(patternString);
			Matcher m = p.matcher(text);
			System.out.println("R.E.: " + patternString);
			System.out.println("Test: " + text);
			boolean found = m.find();
			if (found) {
				System.out.println("Position début : " + m.start());
				System.out.println("Avant : " + text.substring(0,m.start()));
				System.out.println("Sélection : " + m.group());
				// System.out.println("Groupe : " + m.group(1));
				System.out.println("Position fin : " + m.end());
				System.out.println("Après : " + text.substring(m.end()));
				
				}
			System.out.println("\n");
		}
		
		}
}
